VERSION := $(shell grep __version__ orbit_database_shell/__init__.py |cut -d"'" -f2)
.PHONEY: all

all:
	@echo "version"
	@echo "run"


publish:
	@echo "publish - publish client"
	@./scripts/roll_version.py
	@poetry build && poetry publish

publish_local:
	@echo "publish :: server :: local"
	@./scripts/roll_version.py
	@poetry publish --build --repository borg

version:
	curl https://img.shields.io/badge/Version-${VERSION}-teal?style=flat -o images/version.svg

run:
	PYTHONPATH=. python3 orbit_database_shell/__main__.py

backup:
	tar cvfz /tmp/orbit-database-shell.tgz \
		--exclude="*.mdb" \
		--exclude="dist" \
		--exclude="build" \
		--exclude=".cache" \
		--exclude="*.log" \
		--exclude="*.zip" \
		--exclude=".random_errors" \
		--exclude=".pytest*" \
		--exclude="*__pycache__*" \
		--exclude=".attic" \
		--exclude=".tox" \
		--exclude=".venv"  \
		--exclude=".crap"  \
		--exclude="htmlcov"  \
		--exclude=".git" \
		--exclude=".thumbnail_cache" \
		--exclude=".parts_cache" \
		.
